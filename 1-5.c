/*
 * -----------------------------------------------------------------------------
 * Prompt:
 * Modify the temperature conversion program to print the table in reverse
 * order, that is, from 300 degrees to 0.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int   lowerBound, upperBound, step;
  float cls;

  lowerBound  =   0;
  upperBound  = 300;
  step        = -20;
  cls         = 0.0;

  printf("\tFahrenheit to Celsius\n");
  printf("\t---------------------\n");

  for (float fht = upperBound; fht >= lowerBound; fht += step)
  {
    cls = (5.0 / 9.0) * (fht - 32.0);
    printf("\t%5.1f\t\t%5.1f\n", fht, cls);
  }

  return 0;
}
