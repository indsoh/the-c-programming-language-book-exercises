/*
 * -----------------------------------------------------------------------------
 * Prompt: Write a program that prints its input one word per line.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int c, lineCount, wordFlag;

  wordFlag  = 0;

  while ((c = getchar()) != EOF)
  {
    if ((c == ' ') || (c == '\n') || (c == '\t'))
    {
      if (wordFlag == 1)
      {
        putchar('\n');
      }

      lineCount++;
      wordFlag = 0;
    }
    else
    {
      wordFlag = 1;
      putchar(c);
    }
  }

  if (lineCount == 0)
  {
    printf("\n");
  }

  return 0;
}
