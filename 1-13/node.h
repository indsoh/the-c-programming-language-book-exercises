struct node
{
  struct node*  next;
  int           key;
  int           count;
};

void setNext(struct node* n1, struct node* n2);
