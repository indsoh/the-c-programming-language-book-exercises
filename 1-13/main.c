#include <stdlib.h>
#include <stdio.h>
#include "node.h"

int main()
{
  struct node *n1 = malloc(sizeof *n1);
  struct node *n2 = malloc(sizeof *n2);

  n1->next  = NULL;
  n1->key   = 3;
  n1->count = 0;

  n2->next  = NULL;
  n2->key   = 5;
  n2->count = 0;

  setNext(n1, n2);

  printf("%d %d\n", n1->key, n1->count);
  printf("%d %d\n", n1->next->key, n1->next->count);

  free(n1);
  free(n2);
}
