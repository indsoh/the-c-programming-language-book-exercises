/*
 * -----------------------------------------------------------------------------
 * Prompt:
 * Write a program to copy its input to its output, replacing each string of one
 * or more blanks by a single blank.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int c, spaceFlag;

  spaceFlag = 0;

  while ((c = getchar()) != EOF)
  {
    if ((c == ' ') && (spaceFlag == 0))
    {
      spaceFlag = 1;
      putchar(c);
    }
    else if (c != ' ')
    {
      spaceFlag = 0;
      putchar(c);
    }
  }
  
  putchar('\n');
  return 0;
}
