/*
 * -----------------------------------------------------------------------------
 * Prompt:
 * Verify that the expression getchar() != EOF is 0 or 1.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int c;

  while ((c = getchar()) != EOF)
  {
    printf("Reading a character, getchar() != EOF evaluates to ");
    printf("%d\n", c != EOF);
  }
  
  printf("End of file reached, getchar() != EOF evaluates to ");
  printf("%d\n", c != EOF);

  return 0;
}
