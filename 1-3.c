/*
 * -----------------------------------------------------------------------------
 * Prompt:
 * Modify the temperature conversion program to print a heading above the table.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int   lowerBound, upperBound, step;
  float cls;

  lowerBound  =   0;
  upperBound  = 300;
  step        =  20;
  cls         = 0.0;

  printf("\tFahrenheit to Celsius\n");
  printf("\t---------------------\n");

  for (float fht = lowerBound; fht <= upperBound; fht += step)
  {
    cls = (5.0 / 9.0) * (fht - 32.0);
    printf("\t%5.1f\t\t%5.1f\n", fht, cls);
  }

  return 0;
}
