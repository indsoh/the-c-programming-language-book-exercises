/*
 * -----------------------------------------------------------------------------
 * How I would implement the word count program. 
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int c, lineCount, wordCount, charCount, wordFlag;

  lineCount = 0;
  wordCount = 0;
  charCount = 0;
  wordFlag  = 0;

  while ((c = getchar()) != EOF)
  {
    charCount++;
    
    if ((c == ' ') || (c == '\n') || (c == '\t'))
    {
      if (c== '\n')
      {
         lineCount++;
      }

      wordFlag = 0;
    }
    else if (wordFlag == 0)
    {
      wordFlag = 1;
      wordCount++;
    }
  }

  if (lineCount == 0)
  {
    printf("\n");
  }

  printf("Line count: %d\n", lineCount);
  printf("Word count: %d\n", wordCount);
  printf("Char count: %d\n", charCount);

  return 0;
}
