/*
 * -----------------------------------------------------------------------------
 * Prompt:
 * Write a program to count blanks, tabs, and newlines.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int c, nlineCount, spaceCount, tabCount;

  nlineCount  = 0;
  spaceCount  = 0;
  tabCount    = 0;
  
  while ((c = getchar()) != EOF)
  {
    if (c == '\n')
    {
      nlineCount++;
    }
    else if (c == ' ')
    {
      spaceCount++;
    }
    else if (c == '\t')
    {
      tabCount ++;
    }
  }

  if (nlineCount == 0)
  {
    printf("\n");
  }

  printf("Newline count: \t%d\n", nlineCount);
  printf("Space count: \t%d\n",   spaceCount);
  printf("Tab count: \t%d\n",     tabCount);

  return 0;
}
