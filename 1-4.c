/*
 * -----------------------------------------------------------------------------
 * Prompt:
 * Write a program to print the corresponding Celsius to Fahrenheit table.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>

int main()
{
  int   lowerBound, upperBound, step;
  float fht;

  lowerBound  =   0;
  upperBound  = 300;
  step        =  20;
  fht         = 0.0;

  printf("\tCelsius to Fahrenheit\n");
  printf("\t---------------------\n");

  for (float cls = lowerBound; cls <= upperBound; cls += step)
  {
    fht = (9.0 / 5.0) * cls + 32.0;
    printf("\t%5.1f\t\t%5.1f\n", cls, fht);
  }

  return 0;
}
